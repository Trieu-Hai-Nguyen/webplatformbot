'use strict'
const dialogflow = require('dialogflow');
const structjson=require('../chatbot/structjson');
const  config=require('../config/keys');
const mongoose = require('mongoose');
const projectId=config.googleProjectID;
const sessionId = config.dialogFlowSessionID;
const credentials={
    client_email: config.googleClientEmail,
    private_key: config.googlePrivateKey
};
// Create a new session
const sessionClient = new dialogflow.SessionsClient({projectId,credentials});
//const sessionPath = sessionClient.sessionPath(projectId, sessionId); //sessionPath:GlobalConstant

const Registration = mongoose.model('registration');

module.exports={
    textQuery: async function(text, userID, parameters={}){
        let self=module.exports;
        //session path go to request
        const sessionPath = sessionClient.sessionPath(projectId, sessionId + userID); // combine sessionID from config and userId from user
        const request = {
            session: sessionPath,
            queryInput: {
                text: {
                    // The query to send to the dialogflow agent
                    text: text,
                    // The language used by the client (en-US)
                    languageCode: config.dialogFlowSessionLanguageCode,
                },
            },
            queryParams:{
                payload:{
                    data:parameters
                }
            }
        };
        let responses= await sessionClient.detectIntent(request);
        responses= await self.handleAction(responses);
        return  responses;
    },

    eventQuery: async function(event, userID, parameters={}){
        let self=module.exports; //for call method saveRegistration
        let sessionPath = sessionClient.sessionPath(projectId, sessionId + userID);
        const request = {
            session: sessionPath,
            queryInput: {
                event: {
                    // The query to send to the dialogflow agent
                    name: event,
                    parameters: structjson.jsonToStructProto(parameters),
                    // The language used by the client (en-US)
                    languageCode: config.dialogFlowSessionLanguageCode,
                },
            }
        };
        let responses= await sessionClient.detectIntent(request);
        responses= await self.handleAction(responses);
        // responses = self.handleAction(responses);
        return  responses;
    },
    //slot filling base on action
    handleAction: function(responses){
        let self = module.exports;
        let queryResult = responses[0].queryResult;

        switch (queryResult.action) {
            //queryResult.action: take action
            case 'recommendcourses-yes':
                if (queryResult.allRequiredParamsPresent) {
                //   queryResult.allRequiredParamsPresent: take all parameters
                //    prepare schema and model
                    self.saveRegistration(queryResult.parameters.fields);
                }
                break;
        }
        return responses;
    },
    //create a new method: registration object.
    saveRegistration: async function(fields){
        const registration = new Registration({
            name: fields.name.stringValue,
            address: fields.address.stringValue,
            phone: fields.phone.stringValue,
            email: fields.email.stringValue,
            dateSent: Date.now()
        });
        try{
            let reg = await registration.save();
            console.log(reg);
        } catch (err){
            console.log(err);
        }
    }
}
