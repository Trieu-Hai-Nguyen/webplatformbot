import React, {Component} from "react";
import axios from "axios/index";
import { withRouter } from 'react-router-dom'; //that helps us follow user browser history
import  Cookies from 'universal-cookie';
import {v4 as uuid} from 'uuid';
import Message from "./Message";
import Card from './Card';
import QuickReplies from "./QuickReplies";
const cookies = new Cookies(); //create a cookie object

class Chatbot extends Component{
    messagesEnd;
    //-----14. Add auto-focus to text input
    talkInput;
    //-----
    constructor(props){
        super(props);
        // This binding is necessary to make `this` work in the callback
        this._handleInputKeyPress = this._handleInputKeyPress.bind(this);
        this._handleQuickReplyPayload = this._handleQuickReplyPayload.bind(this);
        this.hide = this.hide.bind(this);
        this.show = this.show.bind(this);
        this.state={
            messages:[], //in the state of the chatbot component, we will store messages and other information about the bot
            showBot: true,
            shopWelcomeSent: false, //we'll set the state to true, when user comes to shop
        };
        //create cookie
        if (cookies.get('userID') === undefined) {// cookie does not define
            cookies.set('userID', uuid(), { path: '/' }); //set a cookie by calling cookies.set
        }
        console.log(cookies.get('userID'));
    }
    //TRIEU_SETUP REQUESTS TO BACKEND
    async df_text_query (text) {
        let says = {
            speaks: 'user',
            msg: {
                text : {
                    text: text
                }
            }
        };
        this.setState({ messages: [...this.state.messages, says]});
        //use axios post to post to api/df_text_query
        try{
            const res = await axios.post('/api/df_text_query',  {text, userID: cookies.get('userID')}); //send user id and read the value from a cookie
            //let's add the messages we've received from the bot to state messages also (in the fulfillment array)
            for (let msg of res.data.fulfillmentMessages) {
                console.log(JSON.stringify(msg));
                says = {
                    speaks: 'bot',
                    msg: msg
                };
                //console.log('print at setSate',res.data)
                this.setState({ messages: [...this.state.messages, says]});
            }
        } catch (e) {
            says = {
                speaks: 'bot',
                msg: {
                    text: {
                        text: "I'm having troubles. I need to terminate. will be back later"
                    }
                }
            }
            this.setState({ messages: [...this.state.messages, says]});
            let that = this;
            setTimeout(function(){
                that.setState({ showBot: false})
            }, 2000);

        }
    };

    async df_event_query(event) {
        try {
            const res = await axios.post('/api/df_event_query', {event, userID: cookies.get('userID')});// send event, useriD from cookie
            for (let msg of res.data.fulfillmentMessages) {
                let says = {
                    speaks: 'bot',
                    msg: msg
                }

                this.setState({messages: [...this.state.messages, says]});
            }
        }catch (e) {
            let says = {
                speaks: 'bot',
                msg: {
                    text : {
                        text: "I'm having troubles. I need to terminate. will be back later"
                    }
                }
            }

            this.setState({ messages: [...this.state.messages, says]});
            let that = this;
            setTimeout(function(){
                that.setState({ showBot: false})
            }, 2000);
        }
    };

    resolveAfterXSeconds(x) {
        return new Promise(resolve => {
            setTimeout(() => {
                resolve(x);
            }, x * 1000);
        })
    }

    //FIRSTLY, componentDidMount: ALWAYS RUN WELCOME EVENT
    //componentDidMount is only triggered the first time the component is loaded
    async componentDidMount() {
        // this.df_event_query('Welcome'); // show event welcome to start conversation
        // if (window.location.pathname==="/shop" && !this.state.shopWelcomeSent){
        //     // bot listens user's navigation: if users go to shop, it will active WELCOME_SHOP event.
        //     this.df_event_query("WELCOME_SHOP");
        //     this.setState({shopWelcomeSent:true, showBot:true});
        // };
        switch (window.location.pathname) {
            case '/shop':
                if (!this.state.shopWelcomeSent){
                    await this.resolveAfterXSeconds(1);
                    this.df_event_query("WELCOME_SHOP");
                    this.setState({shopWelcomeSent:true, showBot:true});
                }
                break;
            default:
                this.df_event_query('Welcome');
                break;
        };
        this.props.history.listen(()=>{
            console.log('listening');
            if (this.props.history.location.pathname ==="/shop" && !this.state.shopWelcomeSent){
                this.df_event_query("WELCOME_SHOP");
                this.setState({shopWelcomeSent:true, showBot:true});
            }
        });
    }
    componentDidUpdate() {
        this.messagesEnd.scrollIntoView({ behavior: "smooth" }); // this scroll to just jump to element
        //check focus method
        if ( this.talkInput ) {
            this.talkInput.focus();
        }
    }
    show(event) {
        event.preventDefault();
        event.stopPropagation();
        this.setState({showBot: true});
    }
    hide(event) {
        event.preventDefault();
        event.stopPropagation();
        this.setState({showBot: false});
    }


// render Quick Replies
    _handleQuickReplyPayload(event, payload, text) {
        event.preventDefault();
        event.stopPropagation();
        //for triggering intents after event, such as a button click.
        switch (payload) {
            case 'recommended_yes':
                this.df_event_query('SHOW_RECOMMENDATIONS'); // load event from QuickReplies_payload
                break;
            case 'training_masterclass':
                this.df_event_query('MASTERCLASS');
                break;
            default:
                this.df_text_query(text); //call text query, custom event can trigger this intent
        }

    }

//render card
    renderCards(cards) {
        return cards.map((card, i) => <Card key={i} payload={card.structValue}/>);
    }
    renderOneMessage(message, i) {
        if (message.msg && message.msg.text && message.msg.text.text) {
            return <Message key={i} speaks={message.speaks} text={message.msg.text.text}/>;
        } else if (message.msg && message.msg.payload.fields.cards) { //message.msg.payload.fields.cards.listValue.values
            return <div key={i}>
                <div className="card-panel grey lighten-5 z-depth-1">
                    <div style={{overflow: 'hidden'}}>
                        <div className="col s2">
                            <a href="/" className="btn-floating btn-large waves-effect waves-light red">{message.speaks}</a>
                        </div>
                        <div style={{ overflow: 'auto', overflowY: 'scroll'}}>
                            <div style={{ height: 300, width:message.msg.payload.fields.cards.listValue.values.length * 270}}>
                                {this.renderCards(message.msg.payload.fields.cards.listValue.values)}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        //    implement rendering of Quick replies
        //    check the JSON contains a quick replies
        } else if (message.msg &&
            message.msg.payload &&
            message.msg.payload.fields &&
            message.msg.payload.fields.quick_replies
        ) {
            //render quick replies
            return <QuickReplies
                text={message.msg.payload.fields.text ? message.msg.payload.fields.text : null}
                key={i}
                replyClick={this._handleQuickReplyPayload}
                speaks={message.speaks}
                payload={message.msg.payload.fields.quick_replies.listValue.values}/>;
        }
    }



//--------------


// display messages, include: texts, cards, quick replies
    renderMessages(returnedMessages) {
        if (returnedMessages) {
            return returnedMessages.map((message, i) => {
                return this.renderOneMessage(message, i);
                })
        } else {
            return null;
        }
    }
    _handleInputKeyPress(e) {
        if (e.key === 'Enter') {
            this.df_text_query(e.target.value);
            e.target.value = '';
        }
    }
    render() {
        if(this.state.showBot){
            return (
                <div style={{ minHeight: 500, maxHeight: 470, width:400, position: 'absolute', bottom: 0, right: 0, border: '1px solid lightgray'}}>
                    <nav>
                        <div className="nav-wrapper">
                            {/*bot name*/}
                            <a href="/" className="brand-logo" >WidoBot</a>
                            <ul id="nav-mobile" className="right hide-on-med-and-down">
                                <li><a href="/" onClick={this.hide} style={{fontWeight:"bold"}}>
                                    Hide
                                </a></li>
                            </ul>
                        </div>
                    </nav>
                    {/*the message part*/}
                    <div id="chatbot"  style={{ minHeight: 388, maxHeight: 388, width:'100%', overflow: 'auto'}}>
                        {this.renderMessages(this.state.messages)}
                        <div ref={(el) => { this.messagesEnd = el; }}
                             style={{ float:"left", clear: "both" }}>
                        </div>
                    </div>
                    {/*the input part*/}
                    <div className=" col s12" >
                        <input style={{margin: 0, paddingLeft: '1%', paddingRight: '1%', width: '98%'}} ref={(input) => { this.talkInput = input; }} placeholder="type a message:"  onKeyPress={this._handleInputKeyPress} id="user_says" type="text" />
                    </div>
                </div>
            )
        } else {
            return (
                <div style={{ minHeight: 40, maxHeight: 500, width:400, position: 'absolute', bottom: 0, right: 0, border: '1px solid lightgray'}}>
                    <nav>
                        <div className="nav-wrapper">
                            <a href="/" className="brand-logo">WidoBot</a>
                            <ul id="nav-mobile" className="right hide-on-med-and-down">
                                <li><a href="/" onClick={this.show} style={{fontWeight:"bold"}}>
                                    Show
                                </a></li>
                            </ul>
                        </div>
                    </nav>
                    <div ref={(el) => { this.messagesEnd = el; }}
                         style={{ float:"left", clear: "both" }}>
                    </div>
                </div>
            )
        }
    }
}
//export default Chatbot;
export default withRouter(Chatbot);
