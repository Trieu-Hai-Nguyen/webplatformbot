//the method for handling click event
import React, { Component } from 'react';
import QuickReply from "./QuickReply";


class QuickReplies extends Component {
    constructor(props) {
        super(props);
        this._handleClick = this._handleClick.bind(this);
    }
//_handleClick: will be received event object, payload and text from the button click
    _handleClick(event, payload, text) {
        this.props.replyClick(event, payload, text);
    }

    renderQuickReply(reply, i) {
        return <QuickReply key={i} click={this._handleClick} reply={reply} />;
    }
    //pass in quickReplies,
    renderQuickReplies(quickReplies) {
        if (quickReplies) {
            return quickReplies.map((reply, i) => {
                // map to QuickReply component
                    return this.renderQuickReply(reply, i);
                }
            )
        } else {
            return null;
        }
    }

    render() {
        return (
            <div className="col s12 m8 offset-m2 l6 offset-l3">
                <div className="card-panel grey lighten-5 z-depth-1">
                    <div className="row valign-wrapper" style={{display:"block"}}>
                        <div className="col s2">
                            {/*this.props for class component*/}
                            <a href="/" className="btn-floating btn-large waves-effect waves-light red">{this.props.speaks}</a>
                        </div>
                        <div id="quick-replies" className="col s10">
                            {this.props.text && <p>
                                {/*add method*/}
                                {this.props.text.stringValue}
                            </p>
                            }
                            {this.renderQuickReplies(this.props.payload)}
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default QuickReplies;


