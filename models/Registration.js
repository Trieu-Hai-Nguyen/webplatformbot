
const mongoose = require('mongoose');
const { Schema } = mongoose; //schema class
//start creating a schema
const registrationSchema = new Schema({
    name: String,
    address: String,
    phone: String,
    email: String,
    registerDate: Date
});
mongoose.model('registration', registrationSchema);
