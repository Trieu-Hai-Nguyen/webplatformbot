'use strict';
// import React from 'react';
// import {Payload} from "dialogflow-fulfillment";
const {WebhookClient}=require('dialogflow-fulfillment');
const {Payload} = require('dialogflow-fulfillment');
const mongoose = require('mongoose');
const Demand = mongoose.model('demand');
const Coupon=mongoose.model('coupon');
module.exports = app => {
    app.post('/', async (req, res) => {
        const agent = new WebhookClient({ request: req, response: res });
        function snoopy(agent) {
            agent.add(`Welcome to my Snoopy fulfillment!`);
        }
        function fallback(agent) {
            agent.add(`I didn't understand`);
            agent.add(`I'm sorry, can you try again?`);
        }
        async function travel2(agent){
            Demand.findOne({'location': agent.parameters.location}, function(err, location) {
                //findOne--query command: search ``agent.parameters.location'' from ``location''
                if(location !== null) {
                        location.counter++;
                        location.save();
                } else {
                    const demand= new Demand({location:agent.parameters.location}); //write to mongoDB
                    demand.save();
                }
            });
            let coupon=await Coupon.findOne({'VNLocation':agent.parameters.location});
            // console.log(`tesssssssssssssssssssst ${coupon.Link}`);
            let responseText='';
            if (coupon !== null){
                agent.add(`See more detail about your trip in ${coupon.Link}`);
                //agent.add("Text to be displayed");//"PLATFORM_UNSPECIFIED"
                agent.add(new Payload(
                        "PLATFORM_UNSPECIFIED",
                        {
                            "text": "See more detail about your trip ",
                            "quick_replies": [
                                { "text": "more information", "link":coupon.Link}]
                        },{rawPayload: true, sendAsMessage: true}
                    )
                );
            };
            //let responseText=`See more detail about your trip in ${coupon.Link}`;
            // agent.add(responseText);
        }
        // function travel2(agent){
        //     // let responseText = agent.contexts[0].parameters['location.original'] //for unstructured entities
        //     agent.add(`dia chi la ${responseText}`);
        // }

        let intentMap = new Map();
        intentMap.set('snoopy', snoopy);
        intentMap.set('travel2',travel2);
        intentMap.set('Default Fallback Intent', fallback);
        agent.handleRequest(intentMap);
    });
}